<?php

namespace Drupal\Tests\ggroup\Kernel;

use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRelationship;
use Drupal\Tests\group\Kernel\GroupKernelTestBase;

/**
 * Tests the behavior of subgroup creators.
 *
 * @group group
 */
class SubgroupTest extends GroupKernelTestBase {
  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['ggroup', 'ggroup_test_config'];

  /**
   * The account to use as the group creator.
   *
   * @var \Drupal\group\Entity\GroupTypeInterface
   */
  protected $groupType;

  /**
   * The account to use as the group creator.
   *
   * @var \Drupal\group\Entity\GroupTypeInterface
   */
  protected $subGroupType;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['ggroup_test_config']);
    $this->installSchema('ggroup', 'group_graph');

    $this->entityTypeManager = $this->container->get('entity_type.manager');

    $this->groupType = $this->entityTypeManager->getStorage('group_type')->load('default');
    $this->subGroupType = $this->entityTypeManager->getStorage('group_type')->load('subgroup');
  }

  /**
   * Tests the addition of a group to a group.
   */
  public function testCreateSubgroup() {
    [$group, $subGroup] = $this->addGroup();
    $this->assertNotEmpty($group->getContentByEntityId('ggroup:' . $this->subGroupType->id(), $subGroup->id()), 'Subgroup is group content');
  }

  /**
   * Tests the removing subgroup from group.
   */
  public function testDeleteSubgroupFromGroupRelationship() {
    /* @var Group $subGroup */
    [$group, $sub_group] = $this->addGroup();

    foreach (GroupRelationship::loadByEntity($sub_group) as $group_relationship) {
      $group_relationship->delete();

      $this->assertEquals(\Drupal::service('ggroup.group_hierarchy_manager')->groupHasSubgroup($group, $sub_group), FALSE, 'Subgroup is removed');
    }

  }

  /**
   * Tests the removing subgroup.
   */
  public function testDeleteSubgroup() {
    [$group, $sub_group] = $this->addGroup();

    /* @var Group $subGroup */
    $sub_group->delete();

    $this->assertEquals(\Drupal::service('ggroup.group_hierarchy_manager')->groupHasSubgroup($group, $sub_group), FALSE, 'Subgroup is removed');
  }

  /**
   * Create group and attach subgroup to group.
   *
   * @return array
   *   Return group and subgroup.
   */
  private function addGroup() {
    $group = $this->createGroupByType($this->groupType->id(), ['uid' => $this->getCurrentUser()->id()]);
    /* @var Group $subGroup */
    $sub_group = $this->createGroupByType($this->subGroupType->id(), ['uid' => $this->getCurrentUser()->id()]);

    $group->addRelationship($sub_group, 'ggroup:' . $this->subGroupType->id());

    return [$group, $sub_group];
  }

  /**
   * Creates a group by type.
   *
   * @param string $type
   *   Group type.
   * @param array $values
   *   (optional) The values used to create the entity.
   *
   * @return \Drupal\group\Entity\Group
   *   The created group entity.
   */
  private function createGroupByType(string $type, array $values = []) {
    /* @var Group $group */
    $group = $this->entityTypeManager->getStorage('group')->create($values + [
      'type' => $type,
      'label' => $this->randomMachineName(),
    ]);

    $group->enforceIsNew();
    $group->save();

    return $group;
  }

}
