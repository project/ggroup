<?php

namespace Drupal\ggroup\Plugin\Group\Relation;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\group\Plugin\Attribute\GroupRelationType;
use Drupal\group\Plugin\Group\Relation\GroupRelationBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a group relation for subgroups.
 */
#[GroupRelationType(
  id: 'ggroup',
  entity_type_id: 'group',
  pretty_path_key: 'group',
  label: new TranslatableMarkup('Subgroup'),
  description: new TranslatableMarkup('Adds groups to groups.'),
  reference_label: new TranslatableMarkup('Title'),
  reference_description: new TranslatableMarkup('The title of the subgroup to add to the group'),
  deriver: 'Drupal\ggroup\Plugin\Group\Relation\SubgroupDeriver'
)]
class Subgroup extends GroupRelationBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['entity_cardinality'] = 1;
    $config['creator_wizard'] = 0;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();

    // Disable the entity cardinality field as the functionality of this module
    // relies on a cardinality of 1. We don't just hide it, though, to keep a UI
    // that's consistent with other content enabler plugins.
    $info = $this->t("This field has been disabled by the plugin to guarantee the functionality that's expected of it.");
    $form['entity_cardinality']['#disabled'] = TRUE;
    $form['entity_cardinality']['#description'] .= '<br /><em>' . $info . '</em>';

    $form['creator_wizard'] = [
      '#title' => $this->t('Group creator must complete their membership'),
      '#type' => 'checkbox',
      '#default_value' => $configuration['creator_wizard'] ?? FALSE,
      '#description' => $this->t('This will first show you the form to create the group and then a form to fill out your membership details.<br />You can choose to disable this wizard if you did not or will not add any fields to the membership.<br /><strong>Warning:</strong> If you do have fields on the membership and do not use the wizard, you may end up with required fields not being filled out.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    $dependencies['config'] = ["group.type.{$this->getRelationType()->getEntityBundle()}"];
    return $dependencies;
  }

}
